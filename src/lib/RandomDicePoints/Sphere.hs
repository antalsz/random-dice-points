{-# LANGUAGE FlexibleContexts #-}

module RandomDicePoints.Sphere where

import Data.Random
import Linear

sphereT :: (Distribution Uniform a, Floating a) => a -> RVarT m (V3 a)
sphereT r = do
  θ <- uniformT 0 (2*pi)
  z <- uniformT (-1) 1
  let q = sqrt $ 1 - z*z
  pure $ r *^ V3 (q * cos θ) (q * sin θ) z
