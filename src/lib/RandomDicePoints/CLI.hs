{-# LANGUAGE TypeApplications #-}

module RandomDicePoints.CLI where

import RandomDicePoints.Sphere

import Control.Monad
import Data.Foldable

import Data.IORef

import Linear.V3

import Data.Random
import Data.Random.Source.DevRandom
import Data.Random.Source.StdGen

import Options.Applicative

data Seed = ExplicitSeed !Int
          | RandomSeed
          deriving (Eq, Ord, Show, Read)


getSeed :: Seed -> IO Int
getSeed (ExplicitSeed seed) = pure seed
getSeed RandomSeed          = sampleFrom DevURandom stdUniform

seedParser :: Parser Seed
seedParser = pure RandomSeed <|> subcommands
  where
    seeded      = info (ExplicitSeed <$> argument auto (metavar "SEED"))
                       (progDesc "Generate a picture with a given seed")
    random      = info (pure RandomSeed)
                       (progDesc "Generate a picture with a random seed, and print out that seed")
     
    subcommands = hsubparser $ mconcat [ command "seeded" seeded
                                       , command "random" random ]

argumentsInfo :: ParserInfo Seed
argumentsInfo = info (seedParser <**> helper) (fullDesc <> progDesc helpText)
  where
    helpText = "Random points on a sphere"

mainWith :: Seed -> IO ()
mainWith seed = do
  seedValue <- getSeed seed
  when (seed == RandomSeed) $
    putStrLn $ "Seed: " ++ show seedValue
  gen <- newIORef $ mkStdGen seedValue
  let display (V3 x y z) = putStrLn $ show x ++ " " ++ show y ++ " " ++ show z
  traverse_ display <=< sampleFrom @RVar gen $ replicateM 1000 (sphereT @Double 1)

main :: IO ()
main = mainWith =<< execParser argumentsInfo
